package com.petrovsky;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Threads;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.profile.GCProfiler;
import org.openjdk.jmh.profile.StackProfiler;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import com.google.common.base.Enums;

@BenchmarkMode(Mode.AverageTime)
@Fork(value = 1)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 5, time = 5 )
@State(Scope.Benchmark)
@Threads(10)
public class EnumValueOfBenchmark {

    @Benchmark
    public void guavaBasedEnumValueOfExisting(Blackhole blackhole) {
        blackhole.consume(Enums.getIfPresent(SomeEnum.class, "AD").orNull());
    }

    @Benchmark
    public void guavaBasedEnumValueNonOfExisting(Blackhole blackhole) {
        blackhole.consume(Enums.getIfPresent(SomeEnum.class, "non").orNull());
    }

    @Benchmark
    public void javaBasedEnumValueOfExisting(Blackhole blackhole) {
        blackhole.consume(ConverterUtils.toEnumOrDefault(SomeEnum.class, "AD", null));
    }

    @Benchmark
    public void javaBasedEnumValueOfNonExisting(Blackhole blackhole) {
        blackhole.consume(ConverterUtils.toEnumOrDefault(SomeEnum.class, "non", null));
    }

    @Benchmark
    public void mapBasedEnumValueOfExisting(Blackhole blackhole) {
        blackhole.consume(SomeEnumConverter.convert("AD"));
    }

    @Benchmark
    public void mapBasedEnumValueOfNonExisting(Blackhole blackhole) {
        blackhole.consume(SomeEnumConverter.convert("non"));
    }

    @Benchmark
    public void concurrentMapBasedEnumValueOfExisting(Blackhole blackhole) {
        blackhole.consume(SomeEnumConverter.safeConvert("AD"));
    }

    @Benchmark
    public void concurrentMapBasedEnumValueOfNonExisting(Blackhole blackhole) {
        blackhole.consume(SomeEnumConverter.safeConvert("non"));
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(EnumValueOfBenchmark.class.getSimpleName())
                .addProfiler(StackProfiler.class)
                .addProfiler(GCProfiler.class)
                .build();

        new Runner(opt).run();
    }
}
