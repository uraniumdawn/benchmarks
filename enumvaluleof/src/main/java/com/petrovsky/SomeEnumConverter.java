package com.petrovsky;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SomeEnumConverter {

    private static final Map<String, SomeEnum> ENUM_MAP = Arrays.stream(SomeEnum.values())
            .collect(Collectors.toMap(SomeEnum::name, Function.identity(), (a, b) -> a, ConcurrentSkipListMap::new));

    private static final Map<String, SomeEnum> ENUM_MAP_CONCURRENT = Arrays.stream(SomeEnum.values())
            .collect(Collectors.toConcurrentMap(SomeEnum::name, Function.identity()));

    public static SomeEnum convert(final String input) {
        return ENUM_MAP.get(input);
    }

    public static SomeEnum safeConvert(final String input) {
        return ENUM_MAP_CONCURRENT.get(input);
    }
}