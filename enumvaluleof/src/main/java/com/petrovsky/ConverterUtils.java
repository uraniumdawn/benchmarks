package com.petrovsky;

public class ConverterUtils {

    public static <E extends Enum<E>> E toEnumOrDefault(final Class<E> enumClass, final String input, final E defaultValue) {
        try {
            return Enum.valueOf(enumClass, input);
        } catch (IllegalArgumentException e) {
            return defaultValue;
        }
    }
}
