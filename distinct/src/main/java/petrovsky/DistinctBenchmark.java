package petrovsky;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import one.util.streamex.StreamEx;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

@BenchmarkMode(Mode.AverageTime)
@Fork(value = 2)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@State(Scope.Thread)
public class DistinctBenchmark
{
   @Param({"1", "4500", "4500000"})
   public int unique_count;

   @Param({"2", "1500", "1500000"})
   public int duplicate_count;

   public List<String> container;

   public List<String> getContainer()
   {
      return container;
   }

   private Set<String> generate(int count)
   {
      return Stream.generate(UUID::randomUUID)
            .map(UUID::toString)
            .limit(count)
            .collect(Collectors.toSet());
   }

   @Setup(Level.Iteration)
   public void init()
   {
      container = new ArrayList<>(unique_count + duplicate_count * 2);
      Set<String> duplicates = generate(duplicate_count);
      container.addAll(duplicates);
      container.addAll(duplicates);
      container.addAll(generate(unique_count));
   }

   public <E> Set<E> distinct(List<E> elements, int atLeast)
   {
      while (atLeast > 1)
      {
         Set<E> unique = new HashSet<>();
         List<E> dupe = new LinkedList<>();
         elements.forEach(element -> {
            if (!unique.add(element))
            {
               dupe.add(element);
            }
         });
         return distinct(dupe, --atLeast);
      }
      return new HashSet<>(elements);
   }

   public <T> Map<Integer, Set<T>> distinct(List<T> list)
   {
      HashMap<T, Integer> distinctMap = new HashMap<>();
      for (Object element : list)
      {
         T typedElement = (T) element;
         Integer previous = distinctMap.put(typedElement, 1);
         if (previous != null)
            distinctMap.put(typedElement, previous + 1);
      }

      return distinctMap.entrySet()
            .stream()
            .collect(Collectors.groupingBy(Map.Entry::getValue))
            .entrySet()
            .stream()
            .collect(Collectors.toMap(Map.Entry::getKey,
                  values -> values.getValue()
                        .stream()
                        .map(Map.Entry::getKey)
                        .collect(Collectors.toSet())));
   }

   @Benchmark
   public void streamEx_distinct(Blackhole blackhole)
   {
      blackhole.consume(StreamEx.of(getContainer())
            .distinct(2)
            .toImmutableSet());
   }

   @Benchmark
   public void concurrent_streamEx_distinct(Blackhole blackhole)
   {
      blackhole.consume(StreamEx.of(getContainer())
            .parallel()
            .distinct(2)
            .toImmutableSet());
   }

   @Benchmark
   public void set_recursive_distinct(Blackhole blackhole)
   {
      blackhole.consume(distinct(getContainer(), 2));
   }

   @Benchmark
   public void grouping_by_distinct(Blackhole blackhole)
   {

      blackhole.consume(distinct(getContainer()));
   }
}
